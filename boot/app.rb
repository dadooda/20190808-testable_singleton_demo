
#
# Application boot script.
#

require_relative "env"

# Load libraries.
Dir[File.expand_path("../lib/**/*.rb", __dir__)].each { |fn| require fn }
