
require "active_support/core_ext/object/blank"
require "active_support/core_ext/module/delegation"

module Mode
  require_relative "mode/instance"

  class << self
    delegate *[
      :admin_url,
      :domain,
      :insecure?,
      :language,
      :protocol,
    ], to: :instance

    def instance
      @instance ||= Instance.new
    end
  end
end
