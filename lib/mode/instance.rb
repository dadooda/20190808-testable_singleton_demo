
require_relative "../mode"

module Mode
  class Instance
    # @return [String]
    def admin_url
      @admin_url ||= "#{protocol}://#{domain}/#{language}/admin/"
    end

    # The domain part of the URL.
    # @return [String]
    def domain
      @domain ||= if (s = ENV[vn = "DOMAIN"]).present?
        s
      else
        raise "Environment variable must be set: #{vn}"
      end
    end

    # @return [Boolean] Default is <tt>false</tt>.
    def insecure?
      ENV["INSECURE"] == "true"
    end

    # Website language.
    # @return [String] Default is <tt>"de"</tt>.
    def language
      @language ||= if (s = ENV["LANGUAGE"]).present?
        s
      else
        "de"
      end
    end

    # Request protocol string.
    # @return [String] Default is <tt>"https"</tt> unless #{insecure}.
    def protocol
      @protocol ||= if insecure?
        "http"
      else
        "https"
      end
    end
  end
end
