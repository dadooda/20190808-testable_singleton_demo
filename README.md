
Testable Ruby singleton demo
============================

## Usage

```
$ bundle install
$ bundle exec rspec
```

## Where to look

1. Demo class is in `lib/`.
2. Tests are in `spec/lib/`.

## Implementation notes

1. `Mode` is just an umbrella to delegate a bunch of methods to `#instance`, which is, well, an instance of `Instance`. 😊  
  The caller accesses all settings as `Mode.attribute` and doesn't have to worry about anything else.
2. `Mode::Instance` is a plain Ruby class we instantiate in tests as many times as we want, so testing is precise and straightforward.
3. There are no procedural code sequences of any kind. All attributes are evaluated only as they are needed.
4. `#domain` is an example of a self-validating attribute.  
  Upon first use it validates its environment variable (must not be blank) and raises a meaningful exception otherwise.
5. `#protocol` is an in-between computation which makes decision making more evenly spread between methods.  
  `#admin_url` is interested in a protocol string, whereas the protocol string decides on what it is based on `#insecure?` flag.
6. `#insecure?` is a boolean, therefore there isn't a `||=`.  
  There is a workaround to evaluate and memoize booleans, but it's a bit bulky and slightly out of context of this demo.

## Cheers!

&mdash; Alex Fortuna, &copy; 2019
