
module Mode
  RSpec.describe Instance do
    use_method_discovery :m

    let(:instance) { described_class.new }

    subject { instance.send(m) }

    describe "#admin_url" do
      before :each do
        expect(instance).to receive(:domain).and_return("site.com")
      end

      context "when default" do
        it do
          is_expected.to eq "https://site.com/de/admin/"
        end
      end

      context "when configured" do
        it do
          expect(instance).to receive(:insecure?).and_return(true)
          expect(instance).to receive(:language).and_return("fr")
          is_expected.to eq "http://site.com/fr/admin/"
        end
      end
    end # describe "#admin_url"

    describe "#domain" do
      context "when default" do
        it do
          expect { subject }.to raise_error("Environment variable must be set: DOMAIN")
        end
      end

      context "when set to a blank value" do
        it do
          expect(ENV).to receive(:[]).with("DOMAIN").and_return(" ")
          expect { subject }.to raise_error("Environment variable must be set: DOMAIN")
        end
      end

      context "when set via ENV" do
        it do
          expect(ENV).to receive(:[]).with("DOMAIN").and_return("site.com")
          is_expected.to eq "site.com"
        end
      end
    end # describe "#domain"

    describe "#insecure?" do
      context "when default" do
        it { is_expected.to eq false }
      end

      context "when set via ENV" do
        it do
          expect(ENV).to receive(:[]).with("INSECURE").and_return("true")
          is_expected.to eq true
        end
      end
    end # describe "#insecure?"

    describe "#language" do
      context "when default" do
        it { is_expected.to eq "de" }
      end

      context "when set via ENV" do
        it do
          expect(ENV).to receive(:[]).with("LANGUAGE").and_return("fr")
          is_expected.to eq "fr"
        end
      end
    end # describe "#language"
  end
end
