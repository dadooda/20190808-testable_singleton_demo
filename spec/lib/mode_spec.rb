
RSpec.describe Mode do
  describe ".instance" do
    it { expect(described_class.instance).to be_a described_class::Instance }
  end

  describe "delegation" do
    [
      :admin_url,
      :domain,
      :insecure?,
      :language,
      :protocol,
    ].each do |m|
      it { is_expected.to delegate_method(m).to(:instance) }
    end
  end
end
