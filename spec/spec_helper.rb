
require_relative "../boot/app"

# Add RSpec extensions into these modules in a smart and documentable way.
module SpecSupport
  module ClassMethods; end
  module InstanceMethods; end
end

# Load support files.
Dir[File.expand_path("support/**/*.rb", __dir__)].each { |fn| require fn }

# Extend RSpec.
RSpec.configure do |config|
  config.extend SpecSupport::ClassMethods
  config.include SpecSupport::InstanceMethods
end
